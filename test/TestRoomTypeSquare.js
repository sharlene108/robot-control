describe("Robot World: Room type SQUARE, ", function(){
    beforeEach(function() {
        RobotManager.roomType= 'square';
        RobotManager.roomParameters= [5,5];
        RobotManager.robotPosition= [1,2];
        RobotManager.robotDirection= "N";
        RobotManager.errorMessageNumber= -1;
    });

    it('should getError toBeFalsy', function() {
        expect(RobotManager.getError()).toBeFalsy();
    });

    it('should init toBeTruthy', function() {
        expect(RobotManager.init()).toBeTruthy();
    });

    it('should init toBeFalsy after entering wrong roomType', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomType= 'anything';
        expect(RobotManager.init()).toBeFalsy();
    });

    it('should init toBeFalsy after entering wrong Position', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.robotPosition= [50];
        expect(RobotManager.init()).toBeFalsy();
    });

    it('should init toBeFalsy after entering wrong Direction', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.robotDirection= "*";
        expect(RobotManager.init()).toBeFalsy();
    });

    it('should init toBeFalsy after entering wrong Parameters for the room', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomParameters= [1,'0'];
        expect(RobotManager.init()).toBeFalsy();
    });

    it('should checkCommandString using swedish letters as parameters  toBeTruthy', function() {
        expect(RobotManager.checkCommandString("VGGHGHGHGG")).toBeTruthy();
    });

    it('should checkCommandString using english letters as parameters toBeTruthy', function() {
        expect(RobotManager.checkCommandString("LFFRFRFRFF")).toBeTruthy();
    });

    it('should getRobotsPositionAndDirection toBeDefined', function() {
        expect(RobotManager.getRobotsPositionAndDirection()).toBeDefined();
    });

    it('should getError toBe Room types can only be square or circle', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomType= 'anything';
        expect(RobotManager.init()).toBeFalsy();
        expect(RobotManager.getError()).toBe("Room types can only be square or circle");
    });

    it('should getError toBe Rooms parameters must be the array of two positive elements for room square type', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomParameters= [5,'5'];
        expect(RobotManager.init()).toBeFalsy();
        expect(RobotManager.getError()).toBe("Rooms parameters must be the array of two positive elements for room square type");
    });

    it('should init toBe toBeTruthy for room parameters [5,5]', function() {
        RobotManager.roomParameters= [5,5];
        expect(RobotManager.init()).toBeTruthy();
    });

    it('should getError toBe Robots position must be the array of elements 2 positive number', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.robotPosition= 1;
        expect(RobotManager.init()).toBeFalsy();
        expect(RobotManager.getError()).toBe("Robots position must be the array of elements 2 positive number");
    });

    it('should getError toBe Robots direction must be N, E, S or W', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.robotDirection= "4";
        expect(RobotManager.init()).toBeFalsy();
        expect(RobotManager.getError()).toBe("Robot's direction must be N, E, S or W");
    });

    it('should moveRobot using wrong command string toBeFalsy', function() {
        expect(RobotManager.moveRobot("uDFsT")).toBeFalsy();
    });
});