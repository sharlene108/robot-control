describe("Robot World: Room type CIRCLE, ", function(){
    beforeEach(function() {
        RobotManager.roomType= 'circle';
        RobotManager.roomParameters= 10;
        RobotManager.robotPosition= [0,0];
        RobotManager.robotDirection= "N";
        RobotManager.errorMessageNumber= -1;
    });

    it('should init toBeDefined', function() {
        expect(RobotManager.init).toBeDefined();
    });

    it('should init toBeTruthy', function() {
        expect(RobotManager.init()).toBeTruthy();
    });

    it('should init toBeFalsy after error entry roomParameters', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomParameters= -1;
        expect(RobotManager.init()).toBeFalsy();
    });
    it('should init toBeTruthy when room parameters 9', function() {
        RobotManager.roomParameters= 9;
        expect(RobotManager.init()).toBeTruthy();
    });

    it('should init toBe toBeTruthy for room parameters [-1,-1]', function() {
        RobotManager.robotPosition= [-1,-1];
        expect(RobotManager.init()).toBeTruthy();
    });

    it('should getError toBeFalsy', function() {
        expect(RobotManager.getError()).toBeFalsy();
    });

    it('should checkCommandString for english letters as command parameters toBeTruthy', function() {
        expect(RobotManager.checkCommandString("RRFLFFLRF")).toBeTruthy();
    });

    it('should checkCommandString for swedish letters as command parameters  toBeTruthy', function() {
        expect(RobotManager.checkCommandString("HGHGGHGHG")).toBeTruthy();
    });

    it('should getRobotsPositionAndDirection toBeDefined', function() {
        expect(RobotManager.getRobotsPositionAndDirection()).toBeDefined();
    });

    it('should getError toBe Room parameters must be numbers between 1 and 19 for the room circle Type', function() {
        RobotManager.errorMessageNumber= -1;
        RobotManager.roomParameters= 20;
        expect(RobotManager.init()).toBeFalsy();
        expect(RobotManager.getError()).toBe("Room parameters must be numbers between 1 and 19 for the room circle Type");
    });

    it('should moveRobot using wrong parameters toBeFalsy', function() {
        expect(RobotManager.moveRobot("jkdsfhlsednfdjkSDFSDD")).toBeFalsy();
    });
});