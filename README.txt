# RobotControl
# Programming Assignment
# Author: Sharlene Aminullah
#######Technology Used######
## UI framework : Angular
## CSS: bootstrap-3.3.7-dist
## Test Framework: jasmine-2.5.2
## App logic: Javascript

* UnitTestRunner.html in the root directory runs all the tests using Jasmine framework
* Application logic is written in RobotManager.js
* HTML pages are placed under 'view' folder
* To change the button to square and circle a custom CSS was used that is placed under 'css' folder



#####Assignment Instruction####  
● Use JavaScript in the browser.
● The solution must have automatic tests (e.g. jasmine, mocha, buster or optional).
● The solution should include a README file.
● No requirement for Minification or other packaging.
● Send with your whole git repo to follow the commit log.



####Programming Task: Robot Programming####
The task is to program the control program in a robot. The robot is in a two-dimensional
space that has the following

"interface" (you must not use this interface):
{
   getStartPosition: function () {...} // returns a point
   contains: function (point) {...} // returns a bool
}
...Which point is an object that models a two-dimensional point in space.
(If you find a different design of "room-interface" that you think is better
so it is obviously a good thing)

The robot moves in the room by interpreting a string of commands in Swedish:

   V - Turn left HRS - Turn to the right G - Go forward
   Example String: VGGHGHGHGG

It is also possible to configure the robot so it understands commands in English:

   L - Turn left R - Turn to the right F - Go forward
   Example String: LFFRFRFRFF

When the commands run out so the robot report which box (X, y)it stands on and the direction it is facing.
In the moment of starting the robot is always facing north?.

Example 1: We configure a square room of 5x5 squares and Start Position (1.2)
When giving instructions "HGHGGHGHG" Result: "1 3N"

Example 2.
We configure with a circular room with a radius of 10 squares and the origin in the center of the circle, and the Start position (0.0).
In addition, we configure the robot to English.
When giving instructions "RRFLFFLRF" Result: "3 1E"
We want you to build a UI in the browser for this, with the help of the "MV * -ramverk" (cf.http://todomvc.com/).
Select a frame that you enjoy and that you think is appropriate for the task.
The user must provide to the application input using suitable HTML element.
You need not draw the room and the robot on the screen, but it is enough to report the position and direction via text. Apply styling with the appropriate level of ambition (keep it simple), but it will still look "ok" out.

In addition, consider how you would solve the problem with Progressive Enhancement so that's a bonus.

