angular.module('robotManager', ['ngRoute','RoomTypeProvider'])
    .config( ['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/square', {
                templateUrl: 'app/view/square.html',
                controller:'SquareProvider'
            })
            .when('/circle', {
                templateUrl: 'app/view/circle.html',
                controller:'CircleProvider'
            })
            .otherwise({
                redirectTo: '/square'
            });
    }]);

