var RoomTypeProvider = angular.module("RoomTypeProvider",[]);

RoomTypeProvider.controller("SquareProvider",function($scope){
    $scope.dimension = [1,2,3,4,5,6,7,8,9,10];
    $scope.square = function (roomX, roomY, robotPositionX, robotPositionY, commands){
        RobotManager.errorMessageNumber = -1;
        RobotManager.roomType = 'square';
        RobotManager.roomParameters = [roomX, roomY];
        RobotManager.robotPosition = [robotPositionX,robotPositionY];
        RobotManager.robotDirection = "N";

        if(!RobotManager.init()){
            alert(RobotManager.getError());
        }

        RobotManager.moveRobot(commands);
        $scope.result = RobotManager.getRobotsPositionAndDirection();
    }
});

RoomTypeProvider.controller("CircleProvider",function($scope){
    $scope.dimension = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10];
    $scope.radius= [1,2,3,4,5,6,7,8,9,10];
    $scope.circle = function (roomRadius,robotPositionX,robotPositionY,commands){
        RobotManager.errorMessageNumber = -1;
        RobotManager.roomType = 'circle';
        RobotManager.roomParameters = roomRadius;
        RobotManager.robotPosition = [robotPositionX,robotPositionY];
        RobotManager.robotDirection = "N";

        if(!RobotManager.init()){
            alert(RobotManager.getError());
        }
        RobotManager.moveRobot(commands);
        $scope.result = RobotManager.getRobotsPositionAndDirection();
    }
});