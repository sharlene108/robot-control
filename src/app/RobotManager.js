var RobotManager = {
    roomType: 'square',
    roomParameters: [5,5],
    robotDirection: 'N',
    robotPosition: [1,2],
    possibleDirections: ["N","E","S","W"],
    errorMessageNumber: -1,
    errorMessage: [
        "Wrong string of commands.",
        "The next square is outside the room, and have stopped at square:",
        "Robot's direction must be N, E, S or W",
        "Room types can only be square or circle",
        "Room parameters must be numbers between 1 and 19 for the room circle Type",
        "Rooms parameters must be the array of two positive elements for room square type",
        "Robots position must be the array of elements 2 positive number",
        "Robot's position must be integer array with 2 elements",
        "No position or direction found"
    ],
    squares: [],
    stringCommand: '',
    getError: function(){
        if(this.errorMessageNumber!= -1){
            return this.errorMessage[this.errorMessageNumber];
        }
        return false;
    },
    init: function(){
        var regexpNumber = /^[1-9]$|([1][0-9])$/;

        if(this.roomType !== 'square' && this.roomType !== 'circle'){
            this.errorMessageNumber = 3;
        }else if(this.roomType == 'square'){
            if(!Array.isArray(this.roomParameters) ||
                this.roomParameters.length != 2 ||
                !regexpNumber.test(this.roomParameters[0]) ||
                !Number.isInteger(this.roomParameters[0])  ||
                !regexpNumber.test(this.roomParameters[1]) ||
                !Number.isInteger(this.roomParameters[1])){
                    this.errorMessageNumber = 5;
            }
        }else if(this.roomType == 'circle'){
            var regexpStringNumber = /^[1-9]$|([1][0-9])$/;
            if(!regexpStringNumber.test(this.roomParameters)){
                this.errorMessageNumber = 4;
            }
        }

        if(this.possibleDirections.indexOf(this.robotDirection)== -1){
            this.errorMessageNumber = 2;
        }

        if(this.roomType == 'circle'){
            regexpNumber = /^-?[0-9]$|-?([1][0-9])$/;
        }

        if((!Array.isArray(this.robotPosition) ||  this.robotPosition.length != 2) ||  !regexpNumber.test(this.robotPosition[0]) ||  !Number.isInteger(this.robotPosition[0])  ||  !regexpNumber.test(this.robotPosition[1]) || !Number.isInteger(this.robotPosition[1])){
            this.errorMessageNumber = 6;
            if(this.roomType == 'circle'){
                this.errorMessageNumber = 7;
            }
        }

        if(this.errorMessageNumber!= -1){
            return false;
        }

        var start, firstStop, secondStop;
        if(this.roomType == 'circle'){
            start = -this.roomParameters;
            firstStop = secondStop = this.roomParameters;
        }else {
            start = 0;
            firstStop = this.roomParameters[1];
            secondStop = this.roomParameters[0];
        }

        for(var i = start ; i <= firstStop ; i++){
            this.squares[i] = [];
            for(var j = start ; j <= secondStop ; j++){
                this.squares[i][j] = {
                    X : i,
                    Y : j
                }
            }
        }
        return true;
    },
    turnRight : function(){
        var movePosition = this.possibleDirections.indexOf(this.robotDirection);
        if(movePosition == this.possibleDirections.length -1 ){
            return this.possibleDirections[0];
        }
        return this.possibleDirections[movePosition + 1];
    },
    turnLeft : function(){
        var movePosition = this.possibleDirections.indexOf(this.robotDirection);
        if(movePosition == 0 ){
            return this.possibleDirections[this.possibleDirections.length -1];
        }
        return this.possibleDirections[movePosition - 1];
    },
    moveForward : function(){
        var nextPosition = this.getNextPosition();
        var nextSquare = {
            X : nextPosition[0],
            Y : nextPosition[1]
        };
        if(this.isSquareAvailable(nextSquare)){
            this.robotPosition = nextPosition;
        }else{
            this.errorMessageNumber = 1;
            return false;
        }
        return true;
    },
    getNextPosition : function(){
        var x,y;
        switch (this.robotDirection){
            case "N": x = this.robotPosition[0]; y = this.robotPosition[1] - 1; break;
            case "E": x = this.robotPosition[0] + 1; y = this.robotPosition[1]; break;
            case "W": x = this.robotPosition[0] - 1; y = this.robotPosition[1]; break;
            case "S": x = this.robotPosition[0]; y = this.robotPosition[1] + 1; break;
        }
        return [x,y];
    },
    getRobotsPositionAndDirection : function(){
        if (this.errorMessageNumber <= 1){
            var message = "";
            if(this.errorMessageNumber == 0){
                return this.errorMessage[0];
            }
            if(this.errorMessageNumber == 1){
                message = this.errorMessage[1];
            }
            return message + '[' +  this.robotPosition[0] + "," + this.robotPosition[1] + "] " + this.robotDirection;
        }
        return this.errorMessage[8];
    },
    isSquareAvailable : function (square){
        if(this.roomType == 'circle'){
            square.p1 = {"x":square.X-1/2 , "y":square.Y+1/2};
            square.p2 = {"x":square.X+1/2 , "y":square.Y+1/2};
            square.p3 = {"x":square.X-1/2 , "y":square.Y-1/2};
            square.p4 = {"x":square.X+1/2 , "y":square.Y-1/2};

            var distanceNorth = Math.abs(Math.sqrt( Math.pow( square.p1["x"] , 2) + Math.pow( square.p1["y"] , 2) ));
            var distanceEast = Math.abs(Math.sqrt( Math.pow( square.p2["x"] , 2) + Math.pow( square.p2["y"] , 2) ));
            var distanceSouth = Math.abs(Math.sqrt( Math.pow( square.p3["x"] , 2) + Math.pow( square.p3["y"] , 2) ));
            var distanceWest = Math.abs(Math.sqrt( Math.pow( square.p4["x"] , 2) + Math.pow( square.p4["y"] , 2) ));

            if(distanceNorth <= this.roomParameters && distanceEast <= this.roomParameters && distanceSouth <= this.roomParameters && distanceWest <= this.roomParameters){
                return true;
            }
            return false;
        }else {
            if( square.X < 1 || square.Y < 1 || square.X > this.roomParameters[0] || square.Y > this.roomParameters[1] ){
                return false;
            }
        }
        return true;
    },
    checkCommandString: function(stringCommand){
        var swedishCommand = /^[VGH]*$/;
        var englishCommand = /^[LRF]*$/;

        if(swedishCommand.test(stringCommand) || englishCommand.test(stringCommand)){
            return true;
        }
        this.errorMessageNumber = 0;
        return false;
    },
    moveRobotToOnePosition: function(letter){
        switch (letter){
            case 'L':
            case 'V':
                this.robotDirection = this.turnLeft();
                this.stringCommand += 'V';
                break;
            case 'R':
            case 'H':
                this.robotDirection = this.turnRight();
                this.stringCommand += 'H';
                break;
            case 'F':
            case 'G':
                if(this.moveForward()){
                    this.stringCommand += 'G';
                    return true;
                }else{
                    return false;
                }
                break;
        }
    },
    moveRobot : function(string){
        string = string.toUpperCase();
        if(this.checkCommandString(string)){
            var array = string.split('');
            for(var i=0; i<array.length; i++){
                this.moveRobotToOnePosition(array[i]);
            }
            return true;
        }
        this.errorMessageNumber = 0;
    }
}